var fs = require("fs");

describe('Docler Holding UI Test', function() {

beforeAll(function() {
	 	browser.waitForAngularEnabled(false);
	    browser.get('http://uitest.duodecadits.com/');
});
it('Verify Page Title', function() {
	  
    expect(browser.getTitle()).toBe('UI Testing Site','Home Page : Invalid page title display.');
      
    elm = element(by.xpath(getXpathfromProperty("form_link")));
    
    elm.isPresent().then(function(present) {
	    if (present)
	    {
	        elm.click();
	        console.log("Form link click successfully");	    	
	    }
	    else
	    {
	    	console.log("Form link not present");
	    }
    	}
    	, function(err) {
    		console.log("Form link not present");
    	});  
    expect(browser.getTitle()).toBe('UI Testing Site','Form Page : Invalid page title display.');
}); 

it('Company Logo Verification', function() {
	  elm = element(by.xpath(getXpathfromProperty("home_link")));
	  elm.isPresent().then(function(present) {
		    if (present)
		    {
		        elm.click();
		        console.log("Home link click successfully");		    	
		    } 
		    else
		    {
		    	console.log("Home link not present");
		    }
		    }
		    , function(err) {
			  	console.log("Home link not present");
		  });
	  expect(element(by.xpath(getXpathfromProperty("logo_link"))).isPresent()).toBe(true);
}); 

it('Home Page Navigation Verification', function() {
	 elm = element(by.xpath(getXpathfromProperty("home_link")));
	  elm.isPresent().then(function(present) {
		    if (present)
		    {
		        elm.click();
		        console.log("Home link click successfully");		    	
		    } 
		    else 
		    {
		    	console.log("Home link not present");
		    }
		    }
		    , function(err) {
			  	console.log("Home link not present");
		  });
	  expect(element(by.xpath(getXpathfromProperty("home_welcome_contain"))).isPresent()).toBe(true);	
});
  
it('Home Active Status Verification', function() {
	  expect(element(by.xpath(getXpathfromProperty("home_active_menu"))).isPresent()).toBe(true);
});
  
it('Form Page Navigation Verification', function() {
	elm = element(by.xpath(getXpathfromProperty("form_link")));
    
    elm.isPresent().then(function(present) {
    if (present)
    {
        elm.click();
        console.log("Form link click successfully");    	
    } 
    else
    {
    	console.log("Form link not present");
    }
    }
    , function(err) {
	  	console.log("Form link not present");
    });
    expect(element(by.xpath(getXpathfromProperty("form_contain"))).isPresent()).toBe(true);
});

it('Form Active Status Verification', function() {
	  expect(element(by.xpath(getXpathfromProperty("form_active_menu"))).isPresent()).toBe(true);	
});

it('Error 404 Status Verification', function() {
	
	  elm = element(by.xpath(getXpathfromProperty("error_link")));
	  
	  elm.isPresent().then(function(present) {
		    if (present)
		    {
				elm.click();
				console.log("Error link click successfully");		    	
		    } 
		    else
		    {
		    	console.log("Error link not present");
		    }
		   }, function(err)
		   {
			  	console.log("Error link not present");
		   });
	  
	  var statuscode = browser.getCurrentUrl().then( function( url )
	  {
		  return httpGet(url).then(function(result) {			  
		        return result.statusCode;
		    });		  
	  });
      expect(statuscode).toEqual(404);
});

it('Logo Redirect Home Verification', function() {	
	  browser.navigate().back();	  
	  elm = element(by.xpath(getXpathfromProperty("logo_link")));
	  elm.isPresent().then(function(present) {
		    if (present)
		    {
		        elm.click();
		        console.log("Logo link click successfully");    	
		    } 
		    else
		    {
		    	console.log("Logo link not present");
		    }
		    }
		    , function(err) {
			  	console.log("Logo link not present");
		    });
	  expect(element(by.xpath(getXpathfromProperty("home_welcome_contain"))).isPresent()).toBe(true);
	  console.log("Pass: Home page navigate properly");	  
});

it('Home Page H1 Text Visible Verification', function() {
	  elm = element(by.xpath(getXpathfromProperty("home_link")));	
	  elm.isPresent().then(function(present) {
		    if (present)
		    {
		        elm.click();
		        console.log("Home link click successfully");    	
		    } 
		    else
		    {
		    	console.log("Home link not present");
		    }
		    }
		    , function(err)
		    {
			  	console.log("Home link not present");
		    });
	  var h1text;
	  h1text = element(by.xpath(getXpathfromProperty("home_welcome_contain")));
	  expect(h1text.getText()).toEqual('Welcome to the Docler Holding QA Department');
});

it('Home Page P Tag Text Visible Verification', function() {
	  var ptagtext;
	  ptagtext = element(by.xpath(getXpathfromProperty("subline_paragraph_text")));
	  expect(ptagtext.getText()).toEqual('This site is dedicated to perform some exercises and demonstrate automated web testing.');
});

it('Form Page Input Box & Submit Button Verification', function() {
	  elm = element(by.xpath(getXpathfromProperty("form_link"))).click();
	  elm.isPresent().then(function(present) {
		    if (present)
		    {
		        elm.click();
		        console.log("Form link click successfully");    	
		    } 
		    else
		    {
		    	console.log("Form link not present");
		    }
		    }
		    , function(err)
		    {
			  	console.log("Form link not present");
		    });
	  let list = element.all(by.xpath(getXpathfromProperty("form_name_text_field")));
	  expect(element(by.xpath(getXpathfromProperty("form_name_text_field"))).isPresent()).toBe(true);
	  expect(element(by.xpath(getXpathfromProperty("form_submit_button"))).isPresent()).toBe(true);
});

it('Form Page Input & Output Verification', function() {
	  var inputvalue = ["John", "Sophia","Charlie","Emily"];
	  for (var i = 0; i < inputvalue.length; i++) {
		  var inputname = inputvalue[i];
		  
		  element(by.xpath(getXpathfromProperty("form_name_text_field"))).clear();	
		  element(by.xpath(getXpathfromProperty("form_name_text_field"))).sendKeys(inputname);	
		  element(by.xpath(getXpathfromProperty("form_submit_button"))).click();
		  var outputtext;
		  outputtext = element(by.xpath(getXpathfromProperty("form_output_text")));
		  expect(outputtext.getText()).toEqual('Hello '+inputname+'!');
		  browser.navigate().back();		  
	  };
});

function getXpathfromProperty(key){       
    var rawContent = fs.readFileSync("./application.properties");
    var propertyMap = {};
    var fullContent = rawContent.toString();
    var allPairs = fullContent.split("\n");
    for(var i = 0; i<allPairs.length; i++)
    {
        var keyValue = allPairs[i].split(":");
        propertyMap[keyValue[0]] = keyValue[1].toString();
    }
    return propertyMap[key];
}

function httpGet(siteUrl) {
var http = require('http');
var defer = protractor.promise.defer();

http.get(siteUrl, function(response) {

    var bodyString = '';
    response.setEncoding('utf8');

    response.on("data", function(chunk) {
        bodyString += chunk;
    });

    response.on('end', function() {
        defer.fulfill({
            statusCode: response.statusCode,
            bodyString: bodyString
        });
    });
}).on('error', function(e) {
	console.log(e);
    
    defer.reject("Got http.get error: " + e.message);
    
});

return defer.promise;
};

});
