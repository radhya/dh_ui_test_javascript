var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
exports.config = {
seleniumAddress: 'http://localhost:4444/wd/hub',
specs: ['dockler_ui_testing.js'],
jasmineNodeOpts: {
        defaultTimeoutInterval: 30000,
        includeStackTrace: true,
    },
    framework: 'jasmine2',
    onPrepare: function () {
        browser.driver.manage().window().maximize();
        jasmine.getEnv().addReporter(
                new Jasmine2HtmlReporter({
                  savePath: 'target/screenshots'
                })
              );
    } 
};