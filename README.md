# DH_UI_Test_JavaScript

This is a test project for UI testing for Docler Holding in Selenium Protractor with JavaScript Language.

Protractor : https://www.protractortest.org/
Execute Command: npm install -g protractor

Chrome Browser: https://www.google.com/intl/en_in/chrome/

Step 1: Clone the code from git repository

Step 2: Update Webdriver Version in case running with old chrome version
Execute Command: webdriver-manager update

Step 3: Start selenium server
Execute Command: webdriver-manager start

Step 4: Now go to command prompt and set project repository as based.

Step 5: Run command 'protractor conf.js'. This will start executing test in chrome browser.

Step 6: Once execution finish, you will get new reposiroy target/screenshots. Go to that repositoy and there will be file htmlreport.html, please open to show the test case result.

Note: I have set video 'uitestjavascript.mp4' for execution process and generate report. Please watch it if you not clear with above steps.
For Protractor Automation Tools use official link  https://www.protractortest.org/ about details.






